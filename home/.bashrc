# .bashrc
# by dm0nk 2017

# Dir colors
source ~/.dir_colors

# bash prompt (PS1)
source ~/.bash_prompt

# Load functions and aliases
source ~/.bash_functions_and_aliases

# Enable 256 color bash prompt
export TERM=xterm-256color

# Add binary path in homedir
export PATH=$PATH:~/bin/

# Set vim as default editor
export EDITOR=vim

# Append to the Bash history file, rather than overwriting it
shopt -s histappend;

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
	shopt -s "$option" 2> /dev/null;
done;

# Add tab completion for many Bash commands
if which brew &> /dev/null ; then
	source /usr/local/etc/bash_completion;
elif [ -f /etc/bash_completion ]; then
	source /etc/bash_completion;
fi;

# Enable tab completion for `g` by marking it as an alias for `git`
if type _git &> /dev/null && [ -f /usr/local/etc/bash_completion.d/git-completion.bash ]; then
	complete -o default -o nospace -F _git g;
fi;

# Make the same as above but linux compat
if type _git &> /dev/null && [ -f /etc/bash_completion.d/git ]; then
	complete -o default -o nospace -F _git g;
fi;


# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh;


# Add `killall` tab completion for common apps
complete -o "nospace" -W "Contacts Calendar Dock Finder Mail Safari iTunes SystemUIServer Terminal Twitter" killall;


alias www='cd /var/www/laravel'
